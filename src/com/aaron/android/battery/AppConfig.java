package com.aaron.android.battery;

import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

/**
 * @author wangpu
 * 
 *         AppConfig is a general setting read/write module that will access the
 *         android shared application storage.
 * 
 */
public final class AppConfig {

	public static final String UPGRADE_HOST = "http://wlmzfx.sinaapp.com/android/batterysense/";
	public static final String UPGRADE_VERSION_FILE = "vercode.php";
	public static final String UPGRADE_NEW_APK = "BatterySense.apk";
	public static final String UPDATE_SAVENAME = "BatterySense.apk";

	private Context _context;

	private AppConfig(Context context) {
		_context = context;
		loadConfig();
	}

	private static AppConfig _appConfig = null;

	public static AppConfig getInstance(Context context) {
		if (_appConfig == null) {
			_appConfig = new AppConfig(context);
		}
		return _appConfig;
	}

	private boolean _showStatusBarIcon;

	public boolean isShowStatusBarIcon() {
		return _showStatusBarIcon;
	}

	public void setIsShowStatusBarIcon(boolean shown) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		Editor editor = sharedPreferences.edit();
		editor.putBoolean("ShownStatusBarIcon", shown);
		editor.commit();

		_showStatusBarIcon = shown;
	}

	private boolean _checkForUpdate;

	public boolean isCheckForUpdate() {
		return _checkForUpdate;
	}

	public void setCheckForUpdate(boolean checkForUpdate) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		Editor editor = sharedPreferences.edit();
		editor.putBoolean("CheckForUpdate", checkForUpdate);
		editor.commit();

		this._checkForUpdate = checkForUpdate;
	}

	private Date _lastCheckUpdateTime;
	private static final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;

	public Date getLastCheckUpdateTime() {
		return _lastCheckUpdateTime;
	}

	public void setLastCheckUpdateTime(Date date) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		Editor editor = sharedPreferences.edit();
		editor.putLong("LastCheckForUpdateTime", date.getTime());
		editor.commit();

		this._lastCheckUpdateTime = date;
	}

	public boolean needUpdate() {
		if(!isCheckForUpdate())return false;
		
		long millsDiff = new Date().getTime()
				- getLastCheckUpdateTime().getTime();
		if ((millsDiff / MILLSECS_PER_DAY) > 7) {
			return true;
		}

		return false;
	}

	private SharedPreferences _sharedPreferences;

	public SharedPreferences getSharedPreferences() {
		if (_sharedPreferences == null) {
			_sharedPreferences = _context.getSharedPreferences("perference",
					Context.MODE_PRIVATE);
		}

		_sharedPreferences
				.registerOnSharedPreferenceChangeListener(new OnSharedPreferenceChangeListener() {
					public void onSharedPreferenceChanged(
							SharedPreferences sharedPreferences, String key) {
						loadConfig();
					}
				});

		return _sharedPreferences;
	}

	private void loadConfig() {
		SharedPreferences sharedPreferences = getSharedPreferences();
		_showStatusBarIcon = sharedPreferences.getBoolean("ShownStatusBarIcon",
				false);
		_checkForUpdate = sharedPreferences.getBoolean("CheckForUpdate", true);
		_lastCheckUpdateTime = new Date(sharedPreferences.getLong(
				"LastCheckForUpdateTime", 0));
	}

	public int getVersionCode() {
		return _context.getResources().getInteger(R.integer.app_version_code);
	}

	public String getVersionName() {
		return _context.getResources().getString(R.string.app_version_name);
	}

	public String getAppName() {
		return _context.getResources().getString(R.string.app_name);
	}
}
