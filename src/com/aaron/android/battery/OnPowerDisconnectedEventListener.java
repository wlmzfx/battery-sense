package com.aaron.android.battery;

import com.aaron.android.battery.BatteryInfoReceiver.BatteryData;

import android.content.Context;

public interface OnPowerDisconnectedEventListener {
	void OnPowerDisconnected(Context context, BatteryData batteryData);
}
