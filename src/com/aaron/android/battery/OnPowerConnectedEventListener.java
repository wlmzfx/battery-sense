package com.aaron.android.battery;

import com.aaron.android.battery.BatteryInfoReceiver.BatteryData;

import android.content.Context;

public interface OnPowerConnectedEventListener {
	void OnPowerConnected(Context context, BatteryData batteryData);
}
