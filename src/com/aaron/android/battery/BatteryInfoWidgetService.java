package com.aaron.android.battery;

import android.app.NotificationManager;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public final class BatteryInfoWidgetService extends Service {

	private final static String Tag = "BatteryInfoWidgetReceiver";
	private NotificationManager _notificationManager;
	private BatteryInfoReceiver _batterStatusReceiver = null;

	@Override
	public void onCreate() {
		super.onCreate();
		
		// Initialize the notification manager.
		_notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		_batterStatusReceiver = new BatteryInfoWidgetReceiver();
				
		IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		filter.addAction(Intent.ACTION_POWER_CONNECTED);
		filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
		registerReceiver(_batterStatusReceiver, filter);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		
		return START_STICKY;
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(_batterStatusReceiver);
	}

	
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	private final class BatteryInfoWidgetReceiver extends BatteryInfoReceiver
	{
		@Override
		public void OnBatteryChanged(Context context, BatteryData batteryData) {
			super.OnBatteryChanged(context, batteryData);
			
			try {
				Log.d(Tag, "Write new battery level to widget.");

				if(!batteryData.isPowerConnected()){
					setBatteryWidgetStatus(context, batteryData, getBatteryLevelImageResourceId(batteryData.getLevel()));
				}else{
					OnPowerConnected(context, batteryData);
				}

			} catch (Exception e) {
				Log.d(Tag, "Write new battery level to widget failed", e);
				Toast.makeText(context,
						context.getResources().getText(R.string.error_show),
						1000).show();
			}
		}
		
		@Override
		public void OnPowerConnected(Context context, BatteryData batteryData) {
			super.OnPowerConnected(context, batteryData);
			
			try {
				Log.d(Tag, "Power Connected.");

				setBatteryWidgetStatus(context, batteryData, R.drawable.battery_bg_charging);

			} catch (Exception e) {
				Log.d(Tag, "Handle power connected event to widget failed", e);
				Toast.makeText(context,
						context.getResources().getText(R.string.error_show),
						1000).show();
			}
		};
		
		@Override
		public void OnPowerDisconnected(Context context, BatteryData batteryData) {
			super.OnPowerDisconnected(context, batteryData);
			
			try {
				Log.d(Tag, "Power Disconnected.");

				setBatteryWidgetStatus(context, batteryData, getBatteryLevelImageResourceId(batteryData.getLevel()));

			} catch (Exception e) {
				Log.d(Tag, "Handle power disconnected event to widget failed", e);
				Toast.makeText(context,
						context.getResources().getText(R.string.error_show),
						1000).show();
			}
		};
		
		private void setBatteryWidgetStatus(Context context, BatteryData batteryData, int batteryWidgetBgImageId) {
			RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

			rv.setImageViewResource(R.id.battery_img, batteryWidgetBgImageId);
			rv.setTextViewText(R.id.battery_level, String.format("%1$d%%", batteryData.getLevel()));

			ComponentName batteryWidget = new ComponentName(context, BatteryInfoWidgetProvider.class);
			AppWidgetManager manager = AppWidgetManager.getInstance(context);

			manager.updateAppWidget(batteryWidget, rv);
		}
		
		private int getBatteryLevelImageResourceId(int batteryLevel) {
			if (batteryLevel >= 95) {
				return R.drawable.battery_bg_100;
			} else if (batteryLevel >= 85) {
				return R.drawable.battery_bg_90;
			} else if (batteryLevel >= 75) {
				return R.drawable.battery_bg_80;
			} else if (batteryLevel >= 65) {
				return R.drawable.battery_bg_70;
			} else if (batteryLevel >= 55) {
				return R.drawable.battery_bg_60;
			} else if (batteryLevel >= 45) {
				return R.drawable.battery_bg_50;
			} else if (batteryLevel >= 35) {
				return R.drawable.battery_bg_40;
			} else if (batteryLevel > 25) {
				return R.drawable.battery_bg_30;
			} else if (batteryLevel >= 20) {
				return R.drawable.battery_bg_25;
			} else if (batteryLevel >= 15) {
				return R.drawable.battery_bg_20;
			} else if (batteryLevel >= 5) {
				return R.drawable.battery_bg_10;
			} else if (batteryLevel > 0) {
				return R.drawable.battery_bg_5;
			} else {
				return R.drawable.battery_bg_50;
			}
		}

		@Override
		protected NotificationManager getNotificationManager() {
			return _notificationManager;
		}
	};
}
