package com.aaron.android.battery;

import com.aaron.android.battery.BatteryInfoReceiver.BatteryData;

import android.content.Context;

public interface OnBatteryChangedEventListener {
	void OnBatteryChanged(Context context, BatteryData batteryData);
}
