package com.aaron.android.battery;

import java.io.File;


public interface OnDownloadCompletedListener {
	void OnDownloadCompleted(File saveFile);
}
