package com.aaron.android.battery;

import java.io.File;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ToggleButton;

public class Configuration extends Activity implements OnDownloadCompletedListener {
	
	private final class UpdateButtonOnClickListener implements OnClickListener {
		
		private final class UpdateDialogOKOnClickListener implements
				DialogInterface.OnClickListener {
			public void onClick(DialogInterface arg0, int arg1) {
				_progressBar = new ProgressDialog(Configuration.this);
				_progressBar.setTitle(R.string.app_upgrade_progress_title);
				_progressBar.setIndeterminate(false);
				_progressBar.setMax(100);
				_progressBar.setMessage(getApplicationContext().getResources().getString(R.string.app_upgrade_progress_message));
				_progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				
				String savePath = Environment.getExternalStorageDirectory().getPath() + "/download/" + AppConfig.UPDATE_SAVENAME;
				String severFilePath = AppConfig.UPGRADE_HOST + AppConfig.UPGRADE_NEW_APK;
				DownloadFile download = new DownloadFile(savePath, _progressBar);
				download.setDownloadCompletedListener(Configuration.this);
				download.execute(severFilePath);
			}
		}

		private final class UpdateDialogDismissOnClickListener implements
				DialogInterface.OnClickListener {
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();
			}
		}

		public void onClick(View arg0) {
			final UpgradeHelper upgrade = new UpgradeHelper();
			if(upgrade.fetchServerVersion()){
				int serverVersionCode=upgrade.getServerVersionCode();
				int localVersionCode = AppConfig.getInstance(getApplicationContext()).getVersionCode();
				final Builder dialogBuilder = new AlertDialog.Builder(Configuration.this)
												.setTitle(R.string.app_upgrade_dialog_title)
												.setNegativeButton(R.string.app_upgrade_new_version_dialog_cancel, new UpdateDialogDismissOnClickListener());
				if(localVersionCode < serverVersionCode){
					dialogBuilder.setMessage(String.format(Configuration.this.getResources().getString(R.string.app_upgrade_new_version_text_format), upgrade.getServerVersionName()))
					.setPositiveButton(R.string.app_upgrade_new_version_dialog_ok, new UpdateDialogOKOnClickListener())
					.create().show();
				}else{
					dialogBuilder.setMessage(R.string.app_upgrade_no_new_version_text_format)
					.setPositiveButton(R.string.app_upgrade_no_new_version_dialog_ok, new UpdateDialogDismissOnClickListener())
					.create().show();
				}
			}
		}
	}

	private ProgressDialog _progressBar = null;
	private Handler _handler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.configuration);

		final ToggleButton tb = (ToggleButton) findViewById(R.id.toggle_button_text_shown_status_bar_icon);
		tb.setChecked(AppConfig.getInstance(getApplicationContext()).isShowStatusBarIcon());
		tb.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Context context = getApplicationContext();
				if (tb.isChecked()) {
					AppConfig.getInstance(context).setIsShowStatusBarIcon(true);
					BootCompletedReceiver.startService(context);
				} else {
					AppConfig.getInstance(context)
							.setIsShowStatusBarIcon(false);
					Intent batteryInfoStatusBarIntent = new Intent(context,
							BatteryInfoStatusBarService.class);
					stopService(batteryInfoStatusBarIntent);
				}
			}
		});
		
		final ToggleButton tbUpdate = (ToggleButton) findViewById(R.id.toggle_button_text_check_auto_update);
		tbUpdate.setChecked(AppConfig.getInstance(getApplicationContext()).isCheckForUpdate());
		tbUpdate.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Context context = getApplicationContext();
				if (tbUpdate.isChecked()) {
					AppConfig.getInstance(context).setCheckForUpdate(true);
				} else {
					AppConfig.getInstance(context)
							.setCheckForUpdate(false);
				}
			}
		});
		
		final Button buttonUpdate = (Button)findViewById(R.id.button_check_for_update);
		buttonUpdate.setOnClickListener(new UpdateButtonOnClickListener());
		
		// Automatically update when open the config Activity.
		if(AppConfig.getInstance(getApplicationContext()).needUpdate()){
			buttonUpdate.performClick();
		}
		
		AdView adview = (AdView)findViewById(R.id.adView);
		adview.loadAd(new AdRequest());
	}

	public void OnDownloadCompleted(final File saveFile) {
		_handler.post(new Runnable() {
			public void run() {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(saveFile), "application/vnd.android.package-archive");
				startActivity(intent);
			}
		});
	}
}
