package com.aaron.android.battery;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;

/**
 * @author wangpu
 *
 * The receiver that will receive battery info changes includes:
 * 1. Battery level changes
 * 2. Battery power connected
 * 3. Battery power disconnected
 * Please be aware to filter on only the three kind of Intent.
 * 
 * And this abstract receiver will also convert the Intent info to an internal used BatteryData object that contains all the needed info.
 *
 */
public abstract class BatteryInfoReceiver extends BroadcastReceiver implements
		OnBatteryChangedEventListener, OnPowerConnectedEventListener, OnPowerDisconnectedEventListener{

	public final static int BATTERY_FULL = 100;
	public final static int BATTERY_LOW = 15;
	private final static String Tag = "BatteryInfoReceiver";
	// _lastBatteryData is used to remember the battery info of last update status.
	// 	we need this because the ACTION_POWER_CONNECTED and ACTION_POWER_DISCONNECTED does contain the battery data info
	//		but we cannot update the battery info to all 'zero' on user interface.
	private BatteryData _lastBatteryData = null;	

	protected abstract NotificationManager getNotificationManager();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent == null) {
			return;
		}

		// Monitor the battery changed message.
		String action = intent.getAction();
		Log.d(Tag, "Battery - " + action);
		if(action.equals(Intent.ACTION_BATTERY_CHANGED)){
			BatteryData batteryData = new BatteryData(context, intent);
			this._lastBatteryData = batteryData;
			OnBatteryChanged(context, batteryData);
		}else if(action.equals(Intent.ACTION_POWER_CONNECTED)){
			if(_lastBatteryData == null) {
				return;
			}
			BatteryData batteryData = new BatteryData(_lastBatteryData, true);
			this._lastBatteryData = batteryData;
			OnPowerConnected(context, batteryData);
		}else if(action.equals(Intent.ACTION_POWER_DISCONNECTED)){
			if(_lastBatteryData == null) {
				return;
			}
			BatteryData batteryData = new BatteryData(_lastBatteryData, false);
			this._lastBatteryData = batteryData;
			OnPowerDisconnected(context, batteryData);
		}
	}
	
	
	
	public void OnBatteryChanged(Context context, BatteryData batteryData) {
		// Notify that battery is full of charge.
		if(batteryData.isPowerConnected() && (batteryData.getLevel() == BATTERY_FULL)){
			
		}
		// Notify that battery is low of charge.
		if(!batteryData.isPowerConnected() && (batteryData.getLevel() == BATTERY_LOW)){
						
		}
	}
	
	public void OnPowerConnected(Context context, BatteryData batteryData) {
		// If battery level is higher than 15%, not suggest to charge.
		if(batteryData.getLevel() > 15){
			
		}
		
		// If battery level is lower than 10%, give an suggestion that this kind of charging action is bad to your battery.
		if(batteryData.getLevel() < 10){
			
		}
	}
	
	public void OnPowerDisconnected(Context context, BatteryData batteryData) {
		
		// Give an suggestion if power disconnected when battery is not full. 
		if(batteryData.getLevel() != BATTERY_FULL) {
			
		}
	}
	
	public static class BatteryData{
		private int _level = 0;
		private int _status = BatteryManager.BATTERY_STATUS_UNKNOWN;
		private int _temperature = 0;
		private String _health;
		private boolean _isPowerConnected = false;
		
		public BatteryData(BatteryData batteryData, boolean isPowerConnected){
			this._level = batteryData.getLevel();
			this._status = batteryData.getStatus();
			this._temperature = batteryData.getTemperature();
			this._health = batteryData.getHealth();
			this._isPowerConnected = isPowerConnected;
		}
		
		public BatteryData(Context context, Intent intent){
			int level = intent.getIntExtra("level", 0);
			int scale = intent.getIntExtra("scale", 100);
			this._level = (level * 100) / scale;
			this._status = intent.getIntExtra("status", BatteryManager.BATTERY_STATUS_UNKNOWN);
			this._temperature = (int) (intent.getIntExtra("temperature", 0) * 0.1);
			this._isPowerConnected = intent.getIntExtra("plugged", 0) != 0;
			int health = intent.getIntExtra("health", BatteryManager.BATTERY_HEALTH_UNKNOWN);
			String[] healths = context.getResources().getText(R.string.battery_healths).toString().split(":");
			this._health = (health == BatteryManager.BATTERY_HEALTH_GOOD ? healths[0] : 
				(health == BatteryManager.BATTERY_HEALTH_DEAD) ? healths[1] : healths[2]);
		}
		
		public int getLevel(){
			return this._level;
		}
		
		public int getStatus(){
			return this._status;
		}
		
		public int getTemperature(){
			return this._temperature;
		}
		
		public String getHealth(){
			return this._health;
		}
		
		public boolean isPowerConnected(){
			return this._isPowerConnected;
		}
		
		public void setPowerConnected(boolean powerConnected){
			this._isPowerConnected = powerConnected;
		}
	}
}
