package com.aaron.android.battery;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public final class BatteryInfoStatusBarService extends Service {

	private final static String Tag = "BatteryStatusBarInfoService";
	private NotificationManager _notificationManager;
	
	private final static int REQUEST_CODE = 20000;
	private BatteryInfoReceiver _batterStatusBarInfoReceiver = null;

	@Override
	public void onCreate() {
		super.onCreate();
		// Initialize the notification manager.
		_notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		_batterStatusBarInfoReceiver = new BatteryInfoStatusbarReceiver();
				
		IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		filter.addAction(Intent.ACTION_POWER_CONNECTED);
		filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
		registerReceiver(_batterStatusBarInfoReceiver, filter);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		
		return START_STICKY;
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(_batterStatusBarInfoReceiver);
		_notificationManager.cancel(REQUEST_CODE);
	}
	
	private final class BatteryInfoStatusbarReceiver extends BatteryInfoReceiver{

		public void OnBatteryChanged(Context context, BatteryData batteryData) {
			if (batteryData.getLevel() <= 0) {
				return;
			}
			
			try {
				Log.d(Tag, "Write new battery level to status bar.");

				setBatteryStatusBar(context, batteryData);

			} catch (Exception e) {
				Log.d(Tag, "Write new battery level to status bar failed", e);
				Toast.makeText(context,
						context.getResources().getText(R.string.error_show),
						1000).show();
			}
			
		}
		
		private void setBatteryStatusBar(Context context, BatteryData batteryData) {

			// Add notification to status bar.
			Notification notification = new Notification(
					statusBarIconIds[batteryData.getLevel()], "",
					System.currentTimeMillis());
			Intent notificationIntent = new Intent(context, Configuration.class);
			PendingIntent contentIntent = PendingIntent.getActivity(context,
					10000, notificationIntent, 0);
			notification.setLatestEventInfo(context,
					getResources().getText(R.string.notification_title),
					String.format(getResources().getText(R.string.notification_content).toString(), batteryData.getHealth(), batteryData.getTemperature()),
					contentIntent);
			notification.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;

			_notificationManager.notify(REQUEST_CODE, notification);
		}
		
		private final int statusBarIconIds[] = new int[] { R.drawable.battery_status_bar_00,
				R.drawable.battery_status_bar_01,
				R.drawable.battery_status_bar_02,
				R.drawable.battery_status_bar_03,
				R.drawable.battery_status_bar_04,
				R.drawable.battery_status_bar_05,
				R.drawable.battery_status_bar_06,
				R.drawable.battery_status_bar_07,
				R.drawable.battery_status_bar_08,
				R.drawable.battery_status_bar_09,
				R.drawable.battery_status_bar_10,
				R.drawable.battery_status_bar_11,
				R.drawable.battery_status_bar_12,
				R.drawable.battery_status_bar_13,
				R.drawable.battery_status_bar_14,
				R.drawable.battery_status_bar_15,
				R.drawable.battery_status_bar_16,
				R.drawable.battery_status_bar_17,
				R.drawable.battery_status_bar_18,
				R.drawable.battery_status_bar_19,
				R.drawable.battery_status_bar_20,
				R.drawable.battery_status_bar_21,
				R.drawable.battery_status_bar_22,
				R.drawable.battery_status_bar_23,
				R.drawable.battery_status_bar_24,
				R.drawable.battery_status_bar_25,
				R.drawable.battery_status_bar_26,
				R.drawable.battery_status_bar_27,
				R.drawable.battery_status_bar_28,
				R.drawable.battery_status_bar_29,
				R.drawable.battery_status_bar_30,
				R.drawable.battery_status_bar_31,
				R.drawable.battery_status_bar_32,
				R.drawable.battery_status_bar_33,
				R.drawable.battery_status_bar_34,
				R.drawable.battery_status_bar_35,
				R.drawable.battery_status_bar_36,
				R.drawable.battery_status_bar_37,
				R.drawable.battery_status_bar_38,
				R.drawable.battery_status_bar_39,
				R.drawable.battery_status_bar_40,
				R.drawable.battery_status_bar_41,
				R.drawable.battery_status_bar_42,
				R.drawable.battery_status_bar_43,
				R.drawable.battery_status_bar_44,
				R.drawable.battery_status_bar_45,
				R.drawable.battery_status_bar_46,
				R.drawable.battery_status_bar_47,
				R.drawable.battery_status_bar_48,
				R.drawable.battery_status_bar_49,
				R.drawable.battery_status_bar_50,
				R.drawable.battery_status_bar_51,
				R.drawable.battery_status_bar_52,
				R.drawable.battery_status_bar_53,
				R.drawable.battery_status_bar_54,
				R.drawable.battery_status_bar_55,
				R.drawable.battery_status_bar_56,
				R.drawable.battery_status_bar_57,
				R.drawable.battery_status_bar_58,
				R.drawable.battery_status_bar_59,
				R.drawable.battery_status_bar_60,
				R.drawable.battery_status_bar_61,
				R.drawable.battery_status_bar_62,
				R.drawable.battery_status_bar_63,
				R.drawable.battery_status_bar_64,
				R.drawable.battery_status_bar_65,
				R.drawable.battery_status_bar_66,
				R.drawable.battery_status_bar_67,
				R.drawable.battery_status_bar_68,
				R.drawable.battery_status_bar_69,
				R.drawable.battery_status_bar_70,
				R.drawable.battery_status_bar_71,
				R.drawable.battery_status_bar_72,
				R.drawable.battery_status_bar_73,
				R.drawable.battery_status_bar_74,
				R.drawable.battery_status_bar_75,
				R.drawable.battery_status_bar_76,
				R.drawable.battery_status_bar_77,
				R.drawable.battery_status_bar_78,
				R.drawable.battery_status_bar_79,
				R.drawable.battery_status_bar_80,
				R.drawable.battery_status_bar_81,
				R.drawable.battery_status_bar_82,
				R.drawable.battery_status_bar_83,
				R.drawable.battery_status_bar_84,
				R.drawable.battery_status_bar_85,
				R.drawable.battery_status_bar_86,
				R.drawable.battery_status_bar_87,
				R.drawable.battery_status_bar_88,
				R.drawable.battery_status_bar_89,
				R.drawable.battery_status_bar_90,
				R.drawable.battery_status_bar_91,
				R.drawable.battery_status_bar_92,
				R.drawable.battery_status_bar_93,
				R.drawable.battery_status_bar_94,
				R.drawable.battery_status_bar_95,
				R.drawable.battery_status_bar_96,
				R.drawable.battery_status_bar_97,
				R.drawable.battery_status_bar_98,
				R.drawable.battery_status_bar_99,
				R.drawable.battery_status_bar_100 };

		@Override
		protected NotificationManager getNotificationManager() {
			return _notificationManager;
		}
		
	};
}
